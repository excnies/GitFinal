import argparse
import difflib
import os
import shutil
import hashlib
from datetime import datetime

class TinyGit:
    def __init__(self):
        self.repo_dir = '.tinygit'
        self.stage_dir = os.path.join(self.repo_dir, 'stage')
        self.commits_dir = os.path.join(self.repo_dir, 'commits')
        self.index_file = os.path.join(self.repo_dir, 'index')


    def init_index(self):
        with open(self.index_file, 'w'):
            pass

    def init(self):
        if os.path.isdir(self.repo_dir):
            print("仓库已初始化，无需重复操作")
        else:
            os.makedirs(self.stage_dir, exist_ok=True)
            os.makedirs(self.commits_dir, exist_ok=True)
            self.init_index()  # 初始化索引文件
            print(f"初始化成功，Tiny-Git工作路径： {os.getcwd()}/{self.repo_dir}")

    def add(self, filepath):
        if os.path.isfile(filepath):
            file_hash = self.hash_file(filepath)
            relative_path = os.path.relpath(filepath)
            stage_path = os.path.join(self.stage_dir, relative_path)
            os.makedirs(os.path.dirname(stage_path), exist_ok=True)
            shutil.copy(filepath, stage_path)
            with open(self.index_file, 'a') as index:
                index.write(f"{relative_path} {file_hash}\n")
            print(f"已将 '{relative_path}' 添加到暂存区")
        else:
            print(f"文件 '{filepath}' 不存在")

    def commit(self, message):
        if not message:
            print("提交消息不能为空")
            return

        if not os.listdir(self.stage_dir):
            print("无任何文件提交")
            return

        commit_id = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        commit_path = os.path.join(self.commits_dir, commit_id)
        os.makedirs(commit_path)

        # 暂存区转提交区
        for root, dirs, files in os.walk(self.stage_dir):
            for file in files:
                staged_file_path = os.path.join(root, file)
                commit_file_path = os.path.join(commit_path, os.path.relpath(staged_file_path, self.stage_dir))
                os.makedirs(os.path.dirname(commit_file_path), exist_ok=True)
                os.rename(staged_file_path, commit_file_path)

        with open(os.path.join(commit_path, 'master.txt'), 'w') as file:
            file.write(f"Commit Time: {commit_id}\n")
            file.write(f"Commit Message: {message}\n")

        staging_area_path = '/path/to/your/staging_area'  # 暂存区的路径
        if os.path.exists(staging_area_path):
            shutil.rmtree(staging_area_path)
        print(f"已提交，提交时间 {commit_id}")

    def status(self):
        staged_files = os.listdir(self.stage_dir)
        if not staged_files:
            print("暂存区无文件")
        else:
            print("暂存区未提交的文件：")
            for file in staged_files:
                print(file)

    def hash_file(self, filepath):
        # 计算文件sha1
        hasher = hashlib.sha1()
        with open(filepath, 'rb') as file:
            buf = file.read()
            hasher.update(buf)
        return hasher.hexdigest()



    def help(self):
        print("""
        你可以在 Tiny-Git 运行以下指令:
            init  : 初始化仓库
            commit <提交消息> : 提交暂存区的变更
            status : 显示已暂存和未暂存更改的状态
            add <文件名> : 将文件添加到暂存区
            diff <文件名> : 将工作区中的文件与暂存区中的文件进行比较
            help : 帮助
            exit : 退出
        """)

    def diff(self, filepath):
        relative_path = os.path.relpath(filepath)
        stage_file_path = os.path.join(self.stage_dir, relative_path)

        if not os.path.isfile(filepath):
            print("该文件已被删除。")
            return

        if not os.path.isfile(stage_file_path):
            print("该文件为新增文件。")
            return

        with open(filepath, 'r') as f:
            file_content = f.readlines()

        with open(stage_file_path, 'r') as f:
            stage_content = f.readlines()

        diff = difflib.unified_diff(
            stage_content,
            file_content,
            fromfile="暂存区文件",
            tofile="工作区文件",
            lineterm=''
        )

        diff_output = '\n'.join(list(diff))
        if diff_output:
            print(diff_output)
        else:
            print("文件没有差异。")

    def run(self):
        parser = argparse.ArgumentParser(description='TinyGit')
        parser.add_argument('command', choices=['init', 'add', 'commit', 'status', 'help', 'exit', 'diff'])
        parser.add_argument('args', nargs='*', help='Command arguments')

        while True:
            input_args = input('Tiny-Git > ').strip().split()
            args = input_args[1:] if len(input_args) > 1 else []

            if len(input_args) == 0 or input_args[0] == 'exit':
                break

            command = input_args[0]
            # 定义一个指令到必需参数数量的映射
            required_args = {
                'diff': 1,  # 'diff' 需要一个参数（文件路径）
                'commit': 1,  # 'commit' 需要至少一个参数（提交信息）
                'add': 1  ,# 'commit' 需要至少一个参数（提交信息）
            }

            # 获取所需参数数量，如果指令不在映射中，则默认为 0
            required_count = required_args.get(command, 0)

            # 如果提供的参数数量不满足所需参数数量，则打印错误消息
            if len(args) < required_count:
                # 根据指令选择合适的错误消息
                if command == 'diff':
                    print("错误: 'diff' 命令需要一个文件路径作为参数。")
                elif command == 'commit':
                    print("错误: 'commit' 命令需要一个提交信息作为参数。")
                elif command == 'add':
                    print("错误: 'add' 命令需要一个文件路径作为参数。")
                continue
            if hasattr(self, command):
                try:
                    method = getattr(self, command)
                    method(*args)
                except Exception as e:
                    print(f"发生错误: {e}")
            else:
                print("无效命令，输入“help”获取帮助")
if __name__ == '__main__':
    TinyGit().run()

# Tiny-Git

Tiny-Git是一个使用python编写的仿Git程序。

## 使用

使用IDE或者在终端执行 `python Tiny-Git.py` 运行

你可以在 Tiny-Git 运行以下指令:

-  init  : 初始化仓库
-  commit <提交消息> : 提交暂存区的变更
-  status : 显示已暂存和未暂存更改的状态
-  add <文件名> : 将文件添加到暂存区
-  diff <文件名> : 将工作区中的文件与暂存区中的文件进行比较
-  help : 帮助
-  exit : 退出

在Tiny-Git调用Shell指令可能因平台不同出现不同Bug，对于Tiny-Git外需要命令行的操作，请启用新的会话运行。

## 特点

整个脚本设计成一个命令行界面（CLI），允许用户通过命令与TinyGit交互。

代码中也包含了一些错误处理，并在用户提供了错误的命令或参数数量不足时打印出错误信息。

## 分工

**贾旭**：使用 `argsparse` 等实现用户交互

**赵恒堂：** 实现 `init`，`add`，`commit`等主要功能

**黄琛：**通过`hashlib`等实现对文件的跟踪